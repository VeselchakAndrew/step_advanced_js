import { showCards, getToken } from "./functions.js";
const doctorOption = ["All", "Кардиолог", "Стоматолог", "Терапевт"];
const statusOption = ["All", "Открытый", "Завершен"];
const urgencyOption = ["All", "Обычная", "Приоритетная", "Неотложная"];

class createFilterForm {
    constructor(fieldName, options, selector, type) {
        this.fieldName = fieldName;
        this.options = options;
        this.selector = selector;
        this.type = type;
    }

    render() {
        const element = document.createElement(this.type);
        element.setAttribute("name", this.fieldName);

        if (this.type === "select") {
            this.options.forEach((item) => {
                element.insertAdjacentHTML(
                    "beforeend",
                    `<option>${item}</option>`
                );
            });
        }

        this.selector.append(element);

        return element;
    }
}

function filterForm() {
    const container = document.querySelector(".cards_desk");
    const divFilterForm = document.createElement("div");
    const form = document.createElement("form");
    const titleField = new createFilterForm("purposeVisit", null, form, "input");
    const doctorField = new createFilterForm("doctor", doctorOption, form, "select");
    const statusField = new createFilterForm("status", statusOption, form, "select");
    const urgencyField = new createFilterForm("urgency", urgencyOption, form, "select");
    const filterBtn = document.createElement("button");
    divFilterForm.setAttribute("id", "filterForm");
    form.setAttribute("id", "filters");
    filterBtn.innerText = "Фильтровать";
    filterBtn.setAttribute("id", "filterButton");
    filterBtn.addEventListener("click", filterData);
    form.insertAdjacentElement("beforeend", titleField.render());
    form.insertAdjacentElement("beforeend", doctorField.render());
    form.insertAdjacentElement("beforeend", statusField.render());
    form.insertAdjacentElement("beforeend", urgencyField.render());

    divFilterForm.append(form, filterBtn);
    container.insertAdjacentElement("beforebegin", divFilterForm);

}

function removeFilterForm() {
    const removableElement = document.querySelector("#filterForm");
    if (removableElement) {
        removableElement.remove();
    }
}

function filterData() {
    const token = getToken();
    const form = document.querySelector("#filters");
    const filterObj = {
        purposeVisit: form.elements.purposeVisit.value,
        doctor: form.elements.doctor.value,
        status: form.elements.status.value,
        urgency: form.elements.urgency.value,
    };

    if (form.elements.status.value === "Открытый") {
        filterObj.status = true;
    } else if (form.elements.status.value === "Завершен") {
        filterObj.status = false;
    }
    //Проверка на All и на пустую строку
    for (let key in filterObj) {
        if (filterObj[key] === "All" || filterObj[key] === "") {
            delete filterObj[key];
        }
    }
    event.preventDefault();
    showCards({
        token: token,
    }, filterObj);
}

export { filterForm, removeFilterForm };