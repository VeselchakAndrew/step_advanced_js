import BdWalker from "../classes/BdWalker.js";
import {
    VisitCardTherapist,
    VisitCardCardiologist,
    VisitCardDentist,
} from "../classes/Cards.js";


const walker = new BdWalker();
const token = getToken();
const cards_desk = document.querySelector(".cards_desk");

function getToken() {
    // получить токен из куки
    return getCookie("token");
}

function getLogin() {
    // получить логин из куки
    return getCookie("login");
}

function getCookie(name) {
    //функция для получения значения куки по имени
    let matches = document.cookie.match(
        new RegExp(
            "(?:^|; )" +
            name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
            "=([^;]*)"
        )
    );
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

async function signIn(user) {
    // получить из БД токен  и если успешно - записать токен и логин в куки
    const data = await walker.getToken({
        email: user.login,
        password: user.pass,
    });
    if (data.status !== "Success") {
        console.error(data);
        return data;
    } else {
        document.cookie = `token=${data.token}; path=/; max-age=7200`; // Сохраняю токены в Куки на 2 часа, при повторном вызове время обновляется
        document.cookie = `login=${user.login}; path=/; max-age=7200`;
        await showCards({token: data.token});
        document.querySelector('.login-name').innerHTML = getLogin();
        return data;
    }
}

function signOut() {
    // очистить куки
    document.cookie = `token=''; path=/; max-age=-1`;
    document.cookie = `login=''; path=/; max-age=-1`;
    document.querySelector(".cards_desk").innerHTML = '<p class="main__text">No items have been added</p>';
    document.querySelector('.login-name').innerHTML = '';
    return getCookie("token") === undefined ? 1 : -1;
}

async function createNewCard(data) {
    //повесить на кнопку добавления карты
    await walker.addCard(data);
    await showCards({token: token});
    return data;
}

function cardStatus(card) {
    card.status = true;
    const now = new Date();
    if (new Date(now.getFullYear(), now.getMonth(), now.getDate()) > new Date(card.dateVisit)) {
        card.status = false;
    }
}

function drawCard(card) { // отдельно отрисока полученной карточки
    let data = null;
    if (card.doctor === "Терапевт") {
        data = new VisitCardTherapist(card, walker, token);
    } else if (card.doctor === "Кардиолог") {
        data = new VisitCardCardiologist(card, walker, token);
    } else if (card.doctor === "Стоматолог") {
        data = new VisitCardDentist(card, walker, token);
    } else {
        console.error("Ошибка создания карточки из объекта");
        return;
    }
    if (cards_desk.innerHTML === '<p class="main__text">No items have been added</p>') {
        cards_desk.innerHTML = '';
    }
    data.render(); // Добавляем на страницу
}

function fieldsFilter(obj, word) { // добавлен фильтр по полям ФИО Цель визита и Краткое описание
    const toSearch = word.toLowerCase().trim()
    if (obj.descriptionVisit.toLowerCase().includes(toSearch)) {
        return true;
    } else if (obj.patientName.toLowerCase().includes(toSearch)) {
        return true;
    } else return obj.purposeVisit.toLowerCase().includes(toSearch);
}

async function showCards(params, filter) {
    // получает данные с сервера и создает на основании их карты,
    // для фильтрации - передать объект с полями для фильтра

    let cards = await walker.getCard({token: params.token}); // массив карточек с сервера
    if (cards.length === 0) {
        // если вернулся пустой массив
        cards_desk.innerHTML =
            '<p class="main__text">No items have been added</p>';
        return;
    }
    if (filter) {
        // при наличии объекта фильтра
        cards = cards.filter((card) => {
            cardStatus(card);
            for (const item in filter) {
                if (item === "purposeVisit") {
                    return fieldsFilter(card, filter[item]); // сортировка по нескольким полям
                }
                if (filter[item] !== card[item]) {
                    // если значение поля фильтра не совпадает с полем карточки - ретерним
                    return false; //
                }
            }
            return true; // если поля совпали
        });
    }
    cards_desk.innerHTML = "";
    if (cards.status === "Error") {
        console.error(cards.message);
        cards_desk.innerHTML =
            '<p class="main__text">No items have been added</p>'; // если карточки не добавились
    } else {
        // в зависимости от врача - создаем нужный объект
        for (const card of cards) {
            drawCard(card, walker)
        }
        if (cards_desk.innerHTML === "") {
            cards_desk.innerHTML =
                '<p class="main__text">No items have been added</p>'; // если карточки не добавились
        }
    }
}

export {getToken, signOut, signIn, createNewCard, showCards, drawCard, getLogin};