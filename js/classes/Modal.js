import {getToken, signIn, drawCard, getLogin,} from "../functions/functions.js";
import BdWalker from './BdWalker.js';
import {filterForm} from "../functions/filterform.js";

class NewInput {
    constructor(name, textLabel, appendEl, type, min, max) {
        this.name = name;
        this.textLabel = textLabel;
        this.appendEl = appendEl;
        this.type = type;
        this.min = min;
        this.max = max;
    }

    createInput() {
        let appEl = document.querySelector(`.${this.appendEl}`);
        const labelEl = document.createElement("label");
        labelEl.textContent = `${this.textLabel}`;
        let inputEl = document.createElement("input");
        inputEl.name = this.name;
        inputEl.required = true;
        inputEl.type = this.type;
        inputEl.min = this.min;
        inputEl.max = this.max;
        appEl.append(labelEl);
        appEl.append(inputEl);
    }
}

class NewSelect {
    constructor(name, option, textLabel, appendEl, classEl) {
        this.name = name;
        this.textLabel = textLabel;
        this.appendEl = appendEl;
        this.option = option;
        this.classEl = classEl;
    }

    createSelect() {
        let appEl = document.querySelector(`.${this.appendEl}`);
        const labelEl = document.createElement("label");
        labelEl.textContent = `${this.textLabel}`;
        let selectEl = document.createElement("select");
        selectEl.name = this.name;
        selectEl.classList.add(`${this.classEl}`);
        selectEl.required = true;
        selectEl.insertAdjacentHTML("beforeend", "<option value='' disabled selected>Сделайте выбор</option>");
        this.option.forEach((item) => {
            selectEl.insertAdjacentHTML("beforeend", `<option>${item}</option>`);
        });
        appEl.append(labelEl);
        appEl.append(selectEl);
    }
}

class NewTextArea {
    constructor(name, textLabel, appendEl) {
        this.name = name;
        this.textLabel = textLabel;
        this.appendEl = appendEl;
    }

    createTextArea() {
        let appEl = document.querySelector(`.${this.appendEl}`);
        const labelEl = document.createElement("label");
        labelEl.textContent = `${this.textLabel}`;
        let textareaEl = document.createElement("textarea");
        textareaEl.name = this.name;
        textareaEl.required = true;
        appEl.append(labelEl);
        appEl.append(textareaEl);
    }
}

class VisitDoctor {
    constructor() {
    }

    createVisit() {
        const urgency = ["Обычная", "Приоритетная", "Неотложная"];
        const blockAllEl = document.createElement("fieldset");
        blockAllEl.classList.add("allDoctors");
        let modalForm = document.querySelector(".modalEl");
        modalForm.append(blockAllEl);
        new NewInput("dateVisit", "Дата визита пациента", "allDoctors", "date").createInput();
        new NewTextArea("purposeVisit", "Цель визита", "allDoctors").createTextArea();
        new NewTextArea("descriptionVisit", "Краткое описание", "allDoctors").createTextArea();
        new NewSelect("urgency", urgency, "Выберите срочность", "allDoctors", "selectUrgency").createSelect();
        new NewInput("patientName", "ФИО", "allDoctors").createInput();
    }
}

class VisitCardiologist extends VisitDoctor {
    constructor() {
        super();
    }

    createVisit() {
        super.createVisit();
        const blockPersonally = document.createElement("fieldset");
        blockPersonally.classList.add("personally");
        let selectUrgency = document.querySelector(".selectUrgency");
        selectUrgency.after(blockPersonally);
        new NewInput("normalPressure", "Нормальное давление", "personally", "text", "0").createInput();
        new NewInput("bmi", "Индекс массы тела", "personally", "number", "0", "1000").createInput();
        new NewInput("pastIllness", "Перенесенные заболевания", "personally").createInput();
        new NewInput("age", "Возраст", "personally", "number", "0", "151").createInput();
    }
}

class VisitDentist extends VisitDoctor {
    constructor() {
        super();
    }

    createVisit() {
        super.createVisit();
        const blockPersonally = document.createElement("fieldset");
        blockPersonally.classList.add("personally");
        let selectUrgency = document.querySelector(".selectUrgency");
        selectUrgency.after(blockPersonally);
        new NewInput("dateLastVisit", "Дата последнего визита", "personally", "date").createInput();
    }

}

class VisitTherapist extends VisitDoctor {
    constructor() {
        super();
    }

    createVisit() {
        super.createVisit();
        const blockPersonally = document.createElement("fieldset");
        blockPersonally.classList.add("personally");
        let selectUrgency = document.querySelector(".selectUrgency");
        selectUrgency.after(blockPersonally);
        new NewInput("age", "Возраст", "personally", "number", "1", "120").createInput();
    }
}

class ChangeVisitDoctor {
    constructor(obj) {
    }
}

// Создание формы
class Form {
    constructor() {
        this.formObj = {};
        this.walker = new BdWalker();
        self = this;
        this.token = getToken()
    }

    createForm() {
        let cardsDesk = document.querySelector(".cards_desk");
        const modalDiv = document.createElement("div");
        const modalForm = document.createElement("form");
        const modalEl = document.createElement("fieldset");
        modalEl.classList.add("modalEl");
        modalForm.prepend(modalEl);
        const buttonSubmit = document.createElement("button");
        buttonSubmit.innerHTML = "Submit";
        buttonSubmit.type = "submit";
        modalForm.addEventListener("submit", function (event) {
            event.preventDefault();
            self.submitForm();
        });
        const buttonCancel = document.createElement("button");
        buttonCancel.innerHTML = "Cancel";
        buttonCancel.type = "button";
        buttonCancel.addEventListener("click", () => self.cancel());
        modalDiv.classList.add("modal");
        modalForm.classList.add("modal__form");
        modalForm.id = "formId";
        buttonSubmit.classList.add("button", "butSub");
        buttonCancel.classList.add("button", "butCan");
        modalDiv.append(modalForm);
        modalForm.append(buttonSubmit, buttonCancel);
        let mainEl = document.querySelector(".cards_desk");
        mainEl.append(modalDiv);
    }

    submitForm() {
        let myForm = document.querySelector("#formId");
        let formEl = myForm.elements;
        for (let i = 0; i < formEl.length; i++) {
            if (formEl[i].type !== "fieldset" && formEl[i].type !== "button" && formEl[i].type !== "submit") {
                this.formObj[`${formEl[i].name}`] = formEl[i].value;
            }
        }
    }

    cancel() {
        let divModal = document.querySelector(".modal");
        if (confirm("Карточка не сохранена, удалить введенные данные?")) {
            divModal.remove();
            let divFon = document.querySelector(".divFon");
            divFon.remove();
        }
    }
}

class FormLogin extends Form {
    constructor(appendEl, formObj) {
        super(formObj);
        this.appendEl = appendEl;
        self = this;
    }

    createForm() {
        super.createForm();
        const blockAllEl = document.createElement("fieldset");
        let modalForm = document.querySelector(".modalEl");
        blockAllEl.classList.add("allEl");
        modalForm.append(blockAllEl);
        new NewInput("login", "Введите e-mail", "allEl", "email").createInput();
        new NewInput("pass", "Введите пароль", "allEl", "password").createInput();
    }

    submitForm() {
        super.submitForm();
        self.validate(this.formObj);
        document.querySelector('.login-name').innerHTML = getLogin();
        if (!document.querySelector("#filterForm")) {
            filterForm();
        }
    }

    cancel() {
        let divModal = document.querySelector(".modal");
        let divFon = document.querySelector(".divFon");
        if (divModal) {
            divModal.remove();
        }
        if (divFon) {
            divFon.remove();
        }
    }

    async validate(obj) {
        //Считать логин пароль, проверить валидность
        const answer = await signIn(this.formObj);
        if (answer.status === "Error") {
            let loginNotCorrect = document.createElement("p");
            let blockAllEl = document.querySelector(".allEl");
            loginNotCorrect.classList.add("notCorrect");
            loginNotCorrect.textContent = "Логин или пароль введены не верно!";
            loginNotCorrect.style.margin = "5px";
            blockAllEl.after(loginNotCorrect);
        } else {
            if (document.querySelector(".notCorrect") !== null) {
                document.querySelector(".notCorrect").remove();
            }
            let loginBtn = document.querySelector("#login_button");
            let newVisit = document.querySelector("#newVisit_button");
            let logoutBtn = document.querySelector("#logout_button");
            let divModal = document.querySelector(".modal");
            let divFon = document.querySelector(".divFon");
            loginBtn.style.display = "none";
            newVisit.style.display = "block";
            logoutBtn.style.display = "block";
            if (divModal) {
                divModal.remove();
            }
            if (divFon) {
                divFon.remove();
            }
        }
    }
}

class FormVisitCreate extends Form {
    constructor(appendEl, formObj, board) {
        super(formObj);
        this.appendEl = appendEl;
    }

    createForm() {
        super.createForm();
        const doctors = ["Кардиолог", "Стоматолог", "Терапевт"];
        new NewSelect("doctor", doctors, "Выберите врача", this.appendEl, "selectDoctors").createSelect();
        let selectDoctors = document.querySelector(".selectDoctors");
        selectDoctors.addEventListener("change", function () {
            if (document.querySelector(".allDoctors") !== null) {
                document.querySelector(".allDoctors").remove();
            }
            switch (selectDoctors.selectedIndex) {
                case 1: {
                    new VisitCardiologist().createVisit();
                    break;
                }
                case 2: {
                    new VisitDentist().createVisit();
                    break;
                }
                case 3: {
                    new VisitTherapist().createVisit();
                    break;
                }
            }
        });
    }

    submitForm() {
        super.submitForm();
        let divModal = document.querySelector(".modal");
        divModal.remove();
        let divFon = document.querySelector(".divFon");
        divFon.remove();
        const response = this.walker.addCard({token: this.token, card: this.formObj}).then((answer) => {
            if (answer.status === "Error") {
                console.error("Операция завершилась ошибкой.")
            } else {
                drawCard(answer, this.walker);
            }
        });
    }
}

class FormChangeCard extends Form {
    constructor(obj, appendEl, elem) {
        super();
        this.id = obj.id;
        this.doctor = obj.doctor;
        this.appendEl = appendEl;
        this.obj = obj;
        this.elem = elem;
    }


    createForm() {
        super.createForm();
        let myForm;
        let formEl;

        function fillInForm(myForm, formEl, obj) {
            for (let i = 0; i < formEl.length; i++) {
                if (formEl[i].type !== "fieldset" && formEl[i].type !== "button" && formEl[i].type !== "submit") {
                    let elName = formEl[i].name;
                    formEl[i].value = obj[elName];
                }
            }
        }

        const doctors = ["Кардиолог", "Стоматолог", "Терапевт"];
        new NewSelect("doctor", doctors, "Выберите врача", this.appendEl, "selectDoctors").createSelect();
        let selectDoctors = document.querySelector(".selectDoctors");
        switch (this.doctor) {
            case "Кардиолог": {
                selectDoctors.selectedIndex = 1;
                new VisitCardiologist().createVisit();
                myForm = document.querySelector("#formId");
                formEl = myForm.elements;
                fillInForm(myForm, formEl, this.obj);
                break;
            }
            case "Стоматолог": {
                selectDoctors.selectedIndex = 2;
                new VisitDentist().createVisit();
                myForm = document.querySelector("#formId");
                formEl = myForm.elements;
                fillInForm(myForm, formEl, this.obj);
                break;
            }
            case "Терапевт": {
                selectDoctors.selectedIndex = 3;
                new VisitTherapist().createVisit();
                myForm = document.querySelector("#formId");
                formEl = myForm.elements;
                fillInForm(myForm, formEl, this.obj);
                break;
            }
        }
        selectDoctors.addEventListener("change", () => {
            if (document.querySelector(".allDoctors") !== null) {
                document.querySelector(".allDoctors").remove();
            }
            switch (selectDoctors.selectedIndex) {
                case 1: {
                    new VisitCardiologist().createVisit();
                    fillInForm(myForm, formEl, this.obj);
                    selectDoctors.selectedIndex = 1;
                    break;
                }
                case 2: {
                    new VisitDentist().createVisit();
                    fillInForm(myForm, formEl, this.obj);
                    selectDoctors.selectedIndex = 2;
                    break;
                }
                case 3: {
                    new VisitTherapist().createVisit();
                    fillInForm(myForm, formEl, this.obj);
                    selectDoctors.selectedIndex = 3;
                    break;
                }
            }
        });
    }

    async submitForm() {
        super.submitForm();
        let divModal = document.querySelector(".modal");
        let divFon = document.querySelector(".divFon");
        divModal.remove();
        divFon.remove();
        const response = await this.walker.updateCard({
            token: this.token,
            id: this.id,
            card: this.formObj
        }).then((answer) => {
            if (answer.status === "Error") {
                console.error("Операция завершилась ошибкой.")
            } else {
                this.elem.remove();
                drawCard(answer, this.walker);
            }
        })
    }
}

export {
    NewInput,
    NewSelect,
    NewTextArea,
    VisitDoctor,
    VisitCardiologist,
    VisitDentist,
    VisitTherapist,
    ChangeVisitDoctor,
    Form,
    FormLogin,
    FormVisitCreate,
    FormChangeCard
}