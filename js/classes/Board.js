import BdWalker from "./BdWalker.js";
import {
    VisitCardTherapist,
    VisitCardCardiologist,
    VisitCardDentist,
} from "./Cards.js";

export default class Board {
    constructor() {
        this.walker = new BdWalker();
        this.token = this.getToken();
        this.cards_desk = document.querySelector(".cards_desk");
    }
    getToken() {
        // получить токен из куки
        return this.getCookie("token");
    }

    getLogin() {
        // получить логин из куки
        return this.getCookie("login");
    }

    getCookie(name) {
        //функция для получения значения куки по имени
        let matches = document.cookie.match(
            new RegExp(
                "(?:^|; )" +
                name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
                "=([^;]*)"
            )
        );
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    async signIn(user) {
        // получить из БД токен  и если успешно - записать токен и логин в куки
        const data = await this.walker.getToken({
            email: user.login,
            password: user.pass,
        });
        if (data.status !== "Success") {
            console.error(data);
            return data;
        } else {
            document.cookie = `token=${data.token}; path=/; max-age=7200`; // Сохраняю токены в Куки на 2 часа, при повторном вызове время обновляется
            document.cookie = `login=${user.login}; path=/; max-age=7200`;
            this.showCards({ token: data.token });
            document.querySelector('.login-name').innerHTML = this.getLogin();
            return data;
        }
    }

    signOut() {
        // очистить куки
        document.cookie = `token=''; path=/; max-age=-1`;
        document.cookie = `login=''; path=/; max-age=-1`;
        document.querySelector(".cards_desk").innerHTML = '<p class="main__text">No items have been added</p>';
        document.querySelector('.login-name').innerHTML = '';
        return this.getCookie("token") === undefined ? 1 : -1;
    }

    async createNewCard(data) {
        //повесить на кнопку добавления карты
        await this.walker.addCard(data);
        await this.showCards({ token: this.token });
        return data;
    }

    cardStatus(card) {
        card.status = true;
        const now = new Date();
        if (new Date(now.getFullYear(), now.getMonth(), now.getDate()) > new Date(card.dateVisit)) {
            card.status = false;
        }
    }

    drowCard(card) { // отдельно отрисока полученной карточки
        let data = null;
        if (card.doctor === "Терапевт") {
            data = new VisitCardTherapist(card, this.walker, this.token);
        } else if (card.doctor === "Кардиолог") {
            data = new VisitCardCardiologist(card, this.walker, this.token);
        } else if (card.doctor === "Стоматолог") {
            data = new VisitCardDentist(card, this.walker, this.token);
        } else {
            console.error("Ошибка создания карточки из объекта");
            return;
        }
        data.render(); // Добавляем на страницу
    }

    async showCards(params, filter) {
        // получает данные с сервера и создает на основании их карты,
        // для фильтрации - передать объект с полями для фильтра
        const cards_desk = document.querySelector(".cards_desk"); // Общий див для карточек

        let cards = await this.walker.getCard({ token: this.token }); // массив карточек с сервера
        if (cards.length === 0) {
            // если вернулся пустой массив
            cards_desk.innerHTML =
                '<p class="main__text">No items have been added</p>';
            // removeFilterForm();
            return;
        }
        if (filter) {
            // при наличии объекта фильтра
            cards = cards.filter((card) => {
                for (const item in filter) {
                    if (filter[item] !== card[item]) {
                        // если значение поля фильтра не совпадает с полем карточки - ретерним
                        return false; //
                    }
                }
                return true; // если поля совпали
            });
        }
        cards_desk.innerHTML = "";
        if (cards.status === "Error") {
            console.error(cards.message);
            cards_desk.innerHTML =
                '<p class="main__text">No items have been added</p>'; // если карточки не добавились
        } else {
            // в зависимости от врача - создаем нужный объект
            for (const card of cards) {
                this.drowCard(card, this.walker)
            }
            if (cards_desk.innerHTML === "") {
                cards_desk.innerHTML =
                    '<p class="main__text">No items have been added</p>'; // если карточки не добавились
            }
        }
    }
}