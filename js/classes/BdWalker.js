export default class BdWalker { //Переделал на класс
    constructor() {
        this.API = 'http://cards.danit.com.ua';
        this.LOGIN = '/login';
        this.CARDS = '/cards';
        this.METHODS = {
            post: 'POST',
            get: 'GET',
            put: 'PUT',
            delete: 'DELETE',
        }
    }

    async getToken(credentials) { // функция получения токена
        const response = await fetch(this.API + this.LOGIN, {
            method: this.METHODS.post,
            body: JSON.stringify({
                email: credentials.email,
                password: credentials.password
            })
        });
        return await response.json();
        // для получения токена:
        // function getCookie(name) {
        //     let matches = document.cookie.match(new RegExp(
        //         "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        //     ));
        //     return matches ? decodeURIComponent(matches[1]) : undefined;
        // }
    }

    async _connect(data) { // Универсальный запрос для альнейшего использования, использует токен
        let request = {
            method: data.method,
            headers: {
                Authorization: `Bearer ${data.token}`,
            },
        };
        let link = this.API + this.CARDS;
        if (data.id) { // Если запрос с ID то добавляем
            link = link + '/' + data.id;
        }
        if (data.card) { // Аналогично если есть карточка, то добавляем в запрос
            request.body = JSON.stringify(data.card) // Если запрос типа GET - данного поля не должно быть - ругнется
        }
        const response = await fetch(link, request);
        const content = await response.json()
        console.log(content) // -- > !!!Временно для проверки работоспособности, удалить на релизе!
        return content;
    }

    // ----- Отдельные функции для удаления/изменения/добавления/получения карточек
    async addCard(data) {
        return await this._connect({
            method: this.METHODS.post,
            token: data.token,
            card: data.card,
        })
    }

    async updateCard(data) {
        return await this._connect({
            method: this.METHODS.put,
            token: data.token,
            id: data.id,
            card: data.card,
        })
    }

    async removeCard(data) {
        return await this._connect({
            method: this.METHODS.delete,
            token: data.token,
            id: data.id,
        })
    }

    async getCard(data) {
        return await this._connect({
            method: this.METHODS.get,
            token: data.token,
            id: data.id, // если указан ID, то выдаст карточку по данному ID
        })
    }
}

