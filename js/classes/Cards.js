import {FormChangeCard} from "./Modal.js";
import {getToken} from "../functions/functions.js";

class VisitCard {
    constructor(obj, walker, token) {
        this.id = obj.id;
        this.patientName = obj.patientName;
        this.age = obj.age;
        this.doctor = obj.doctor;
        this.purposeVisit = obj.purposeVisit;
        this.descriptionVisit = obj.descriptionVisit;
        this.urgency = obj.urgency;
        this.dateVisit = obj.dateVisit;
        this.cardwalker = walker;
        this.token = token;
        this.cardStatus();
    }

    cardStatus() {
        this.status = true;
        const now = new Date();
        if (new Date(now.getFullYear(), now.getMonth(), now.getDate()) > new Date(this.dateVisit)) {
            this.status = false;
        }
    }


    createCard() {
        // Elements
        this.container = document.querySelector(".cards_desk");
        this.card = document.createElement("div");
        this.nameField = document.createElement("p");
        this.ageField = document.createElement("p");
        this.doctorField = document.createElement("p");
        this.urgencyField = document.createElement("p");
        this.purposeVisitField = document.createElement("p");
        this.descriptionVisitField = document.createElement("p");
        this.dateVisitField = document.createElement("p");
        this.statusField = document.createElement("p");
        // Buttons
        this.close = document.createElement("div");
        this.loadMoreBtn = document.createElement("button");
        this.editBtn = document.createElement("button");
        this.removeBtn = document.createElement("button");
        this.nameField.innerText = `ФИО: ${this.patientName}`;
        this.doctorField.innerText = `Доктор: ${this.doctor}`;
        this.ageField.innerText = `Возраст: ${this.age}`;
        this.urgencyField.innerText = `Срочность: ${this.urgency}`;
        this.purposeVisitField.innerText = `Цель визита: ${this.purposeVisit}`;
        this.descriptionVisitField.innerText = `Краткое описание: ${this.descriptionVisit}`;
        this.dateVisitField.innerText = `Дата визита: ${this.dateVisit}`;
        if (this.status === false) {
            this.statusField.innerText = `Статус: Завершен`;
            this.statusField.classList.add("done");
        } else {
            this.statusField.innerText = `Статус: Открытый`;
            this.statusField.classList.add("inprogress");
        }

        this.card.classList.add("cards");
        this.close.classList.add("close");
        this.ageField.classList.add("hidden");
        this.urgencyField.classList.add("hidden");
        this.purposeVisitField.classList.add("hidden");
        this.descriptionVisitField.classList.add("hidden");
        this.ageField.setAttribute("data-field", "hidden");
        this.urgencyField.setAttribute("data-field", "hidden");
        this.purposeVisitField.setAttribute("data-field", "hidden");
        this.descriptionVisitField.setAttribute("data-field", "hidden");
        this.loadMoreBtn.innerText = "Показать больше";
        this.editBtn.innerText = "Редактировать";
        this.editBtn.addEventListener("click", () => {
            let body = document.querySelector("body");
            let divFon = document.createElement("div");
            divFon.classList.add("divFon");
            body.append(divFon);
            getToChange();
        });
        if (this.status === false) {
            this.editBtn.disabled = true;
        }
        const getToChange = async () => {
            const obj = await this.cardwalker.getCard({
                token: this.token,
                id: this.id,
            });
            let form = new FormChangeCard(obj, "modalEl", this.card);
            form.createForm();
        };

        this.loadMoreBtn.addEventListener("click", () => {
            const currentCard = this.loadMoreBtn.closest(".cards");
            const hiddenFields = currentCard.querySelectorAll("p[data-field='hidden']");
            for (let i = 0; i < hiddenFields.length; i++) {
                hiddenFields[i].classList.toggle("hidden");
            }
            if (this.loadMoreBtn.innerText === "Показать больше") {
                this.loadMoreBtn.innerText = "Скрыть";
            } else this.loadMoreBtn.innerText = "Показать больше";
        });
    }

    render() {
        this.createCard();
        this.close.addEventListener("click", () => {
            // Обработка кнопки закрытия карточки
            const result = confirm("Вы уверены?");
            if (result) {
                const response = this.remove().then((answer) => {
                    if (answer.status === "Success") {
                        this.card.remove();
                        if (document.querySelectorAll(".cards").length === 0) {
                            this.container.innerHTML = '<p class="main__text">No items have been added</p>';
                        }
                    } else {
                        this.container.innerHTML = answer;
                    }
                });
            } else {
                return;
            }
        });

        this.card.append(
            this.statusField,
            this.nameField,
            this.ageField,
            this.doctorField,
            this.urgencyField,
            this.dateVisitField,
            this.purposeVisitField,
            this.descriptionVisitField,
            this.loadMoreBtn,
            this.editBtn,
            this.close
        );
        this.container.append(this.card);
    }

    async remove() {
        const token = getToken();
        // Удаление карточки из бд, использует ID данного объекта
        return await this.cardwalker.removeCard({
            id: this.id,
            token: token,
        });
    }
}

class VisitCardTherapist extends VisitCard {
    constructor(obj, walker, token) {
        super(obj, walker, token);
    }
}

class VisitCardCardiologist extends VisitCard {
    constructor(obj, walker, token) {
        super(obj, walker, token);
        this.normalPressure = obj.normalPressure;
        this.bmi = obj.bmi;
        this.pastIllness = obj.pastIllness;
    }

    createCard() {
        this.normalPressureField = document.createElement("p");
        this.bmiField = document.createElement("p");
        this.pastIllnessField = document.createElement("p");
        this.normalPressureField.innerText = `Нормальное давление:  ${this.normalPressure}`;
        this.bmiField.innerText = `Индекс массы тела:  ${this.bmi}`;
        this.pastIllnessField.innerText = `Предыдущие болезни:  ${this.pastIllness}`;
        this.normalPressureField.classList.add("hidden");
        this.bmiField.classList.add("hidden");
        this.pastIllnessField.classList.add("hidden");
        this.normalPressureField.setAttribute("data-field", "hidden");
        this.bmiField.setAttribute("data-field", "hidden");
        this.pastIllnessField.setAttribute("data-field", "hidden");
        super.createCard();
    }

    render() {
        super.render();
        this.card.append(
            this.nameField,
            this.ageField,
            this.doctorField,
            this.urgencyField,
            this.dateVisitField,
            this.purposeVisitField,
            this.descriptionVisitField,
            this.bmiField,
            this.normalPressureField,
            this.pastIllnessField,
            this.loadMoreBtn,
            this.editBtn,
            this.close
        );
        this.container.append(this.card);
    }
}

class VisitCardDentist extends VisitCard {
    constructor(obj, walker, token) {
        super(obj, walker, token);
        this.dateLastVisit = obj.dateLastVisit;
    }

    createCard() {
        this.dateLastVisitField = document.createElement("p");
        this.dateLastVisitField.innerText = `Дата последнего визита:  ${this.dateLastVisit}`;
        this.dateLastVisitField.classList.add("hidden");
        this.dateLastVisitField.setAttribute("data-field", "hidden");
        super.createCard();
    }

    render() {
        super.render();
        this.card.append(
            this.nameField,
            this.ageField,
            this.doctorField,
            this.urgencyField,
            this.dateVisitField,
            this.purposeVisitField,
            this.descriptionVisitField,
            this.dateLastVisitField,
            this.loadMoreBtn,
            this.editBtn,
            this.close
        );
        this.container.append(this.card);
    }
}

export {VisitCardTherapist, VisitCardCardiologist, VisitCardDentist};