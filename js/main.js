import {
    getLogin,
    getToken,
    signOut,
    showCards,
} from "./functions/functions.js";
import {
    FormLogin,
    FormVisitCreate,
} from "./classes/Modal.js";
import { filterForm, removeFilterForm } from "./functions/filterform.js";

const loginBtn = document.querySelector("#login_button");
const newVisit = document.querySelector("#newVisit_button");
const logoutBtn = document.querySelector("#logout_button");

if (getToken()) {
    document.querySelector('.login-name').innerHTML = getLogin();
    const token = getToken();
    loginBtn.style.display = "none";
    newVisit.style.display = "block";
    logoutBtn.style.display = "block";
    showCards({ token: token });
    if (!document.querySelector("#filterForm")) {
        filterForm();
    }
} else {
    out();
}

newVisit.addEventListener("click", () => {
    let divFon = document.createElement("div");
    divFon.classList.add("divFon");
    let body = document.querySelector("body");
    body.append(divFon);
    let divModal = document.querySelector(".modal");
    let form = new FormVisitCreate("modalEl");
    if (divModal !== null) {
        if (confirm("Карточка не сохранена, удалить введенные данные?")) {
            divModal.remove();
            form.createForm();
        }
    } else {
        form.createForm();
    }
});

loginBtn.addEventListener("click", () => {
    let body = document.querySelector("body");
    let divFon = document.createElement("div");
    divFon.classList.add("divFon");
    body.append(divFon);
    let form = new FormLogin("modalEl");
    form.createForm();


});

logoutBtn.addEventListener("click", () => {
    out();
});

function out() {
    loginBtn.style.display = "block";
    newVisit.style.display = "none";
    logoutBtn.style.display = "none";
    signOut();
    removeFilterForm();
    document.querySelector('.login-name').innerHTML = '';
    //удаляется блок с карточками
}

// const cred = {
//     // для тестов, в дальнейшем эти данные заираются из полей логина и пароля
//     login: "al@ukr.net",
//     pass: "12345",
// };