import BdWalker from "./functions/BdWalker.js";
import { getToken, signIn } from "./functions/functions.js";

const walker = new BdWalker();

const newVisit = document.querySelector("#newVisit_button");
newVisit.addEventListener("click", () => {
    let form = new FormVisitCreate("modalEl");
    return form.createForm();
});
let formObj = {};


class NewInput {
    constructor(name, textLabel, appendEl) {
        this.name = name;
        this.textLabel = textLabel;
        this.appendEl = appendEl;
    }
    createInput() {
        let appEl = document.querySelector(`.${this.appendEl}`);
        const labelEl = document.createElement("label");
        labelEl.textContent = `${this.textLabel}`;
        let inputEl = document.createElement("input");
        inputEl.name = this.name;
        inputEl.required = true;
        appEl.append(labelEl);
        appEl.append(inputEl);
    }
}
class NewSelect {
    constructor(name, option, textLabel, appendEl, classEl) {
        this.name = name;
        this.textLabel = textLabel;
        this.appendEl = appendEl;
        this.option = option;
        this.classEl = classEl;
    }
    createSelect() {
        console.log(this.appendEl);
        let appEl = document.querySelector(`.${this.appendEl}`);
        const labelEl = document.createElement("label");
        labelEl.textContent = `${this.textLabel}`;
        let selectEl = document.createElement("select");
        selectEl.name = this.name;
        selectEl.classList.add(`${this.classEl}`);
        selectEl.insertAdjacentHTML("beforeend", "<option disabled selected>Сделайте выбор</option>");
        this.option.forEach((item) => {
            selectEl.insertAdjacentHTML("beforeend", `<option>${item}</option>`);
        });
        appEl.append(labelEl);
        appEl.append(selectEl);
    }
}
class NewTextArea {
    constructor(name, textLabel, appendEl) {
        this.name = name;
        this.textLabel = textLabel;
        this.appendEl = appendEl;
    }
    createTextArea() {
        let appEl = document.querySelector(`.${this.appendEl}`);
        const labelEl = document.createElement("label");
        labelEl.textContent = `${this.textLabel}`;
        let textareaEl = document.createElement("textarea");
        textareaEl.name = this.name;
        appEl.append(labelEl);
        appEl.append(textareaEl);
    }
}

class VisitDoctor {
    constructor() {}
    createVisit() {
        const urgency = ["Обычная", "Приоритетная", "Неотложная"];
        const blockAllEl = document.createElement("fieldset");
        blockAllEl.classList.add("allDoctors");
        let modalForm = document.querySelector(".modalEl");
        console.log(modalForm);
        modalForm.append(blockAllEl);
        new NewTextArea("purposeVisit", "Цель визита", "allDoctors").createTextArea();
        new NewTextArea("descriptionVisit", "Краткое описание", "allDoctors").createTextArea();
        new NewSelect("urgency", urgency, "Выберите срочность", "allDoctors", "selectUrgency").createSelect();
        new NewInput("patientName", "ФИО", "allDoctors").createInput();
    }


}

class VisitCardiologist extends VisitDoctor {
    constructor() {
        super();
    }
    createVisit() {
        // new NewInput("normalPressure", "Нормальное давление", "cards_desk").createInput();
        // new NewInput("normalPressure", "Нормальное давление", "cards_desk").createInput();
        // const input= new NewInput("normalPressure", "Нормальное давление","cards_desk").createInput();
        // new NewSelect("normalPressure",["Кардиолог", "Стоматолог", "Терапевт"],"Выберите врача", "cards_desk").createSelect();
        // new NewTextArea("normalPressure", "text", "cards_desk").createTextArea();
        //
        super.createVisit();
        const blockPersonally = document.createElement("fieldset");
        blockPersonally.classList.add("personally");
        let selectUrgency = document.querySelector(".selectUrgency");
        selectUrgency.after(blockPersonally);
        new NewInput("normalPressure", "Нормальное давление", "personally").createInput();
        new NewInput("bmi", "Индекс массы тела", "personally").createInput();
        new NewInput("pastIllnesses", "Перенесенные заболевания", "personally").createInput();
        new NewInput("age", "Возраст", "personally").createInput();
    }
}
class VisitDentist extends VisitDoctor {
    constructor() {
        super();
    }
    createVisit() {
        super.createVisit();
        const blockPersonally = document.createElement("fieldset");
        blockPersonally.classList.add("personally");
        let selectUrgency = document.querySelector(".selectUrgency");
        selectUrgency.after(blockPersonally);
        new NewInput("dateLastVisit", "Дата последнего визита", "personally").createInput();
    }

}
class VisitTherapist extends VisitDoctor {
    constructor() {
        super();
    }
    createVisit() {
        super.createVisit();
        const blockPersonally = document.createElement("fieldset");
        blockPersonally.classList.add("personally");
        let selectUrgency = document.querySelector(".selectUrgency");
        selectUrgency.after(blockPersonally);
        new NewInput("age", "Возраст", "personally").createInput();
    }
}


class Form {
    createForm() {
        const modalDiv = document.createElement("div");
        const modalForm = document.createElement("form");
        const modalEl = document.createElement("fieldset");
        modalEl.classList.add("modalEl");
        modalForm.prepend(modalEl);
        const buttonSubmit = document.createElement("button");
        buttonSubmit.innerHTML = "Submit";
        buttonSubmit.type = "submit";
        modalForm.addEventListener("submit", function(event) {
            event.preventDefault();
            form.submitForm();
        });
        const buttonCancel = document.createElement("button");
        buttonCancel.innerHTML = "Cancel";
        buttonCancel.type = "button";
        buttonCancel.addEventListener("click", () => form.cancel());
        modalDiv.classList.add("modal");
        modalForm.classList.add("modal__form");
        modalForm.id = "formId";
        buttonSubmit.classList.add("button", "butSub");
        buttonCancel.classList.add("button", "butCan");
        modalDiv.append(modalForm);
        modalForm.append(buttonSubmit, buttonCancel);
        let cardsDesk = document.querySelector(".cards_desk");
        cardsDesk.append(modalDiv);
    }

    submitForm() {
        let myForm = document.querySelector("#formId");
        let formEl = myForm.elements;
        let buttonSubmit = document.querySelector(".btnSub");
        for (let i = 0; i < formEl.length; i++) {
            if (formEl[i].type !== "fieldset" && formEl[i].type !== "button" && formEl[i].type !== "submit") {
                formObj[`${formEl[i].name}`] = formEl[i].value;
            }
        }

        signIn("al@ukr.net", '12345');
        walker.addCard({
            token: getToken(),
            card: formObj,
        })
    }

    cancel() {
        let divModal = document.querySelector(".modal");
        divModal.remove();
    }
}

class FormLogin extends Form {
    constructor() {
        super();
    }
    validate() {
        //Считать логин пароль, проверить валидность
    }
}

class FormVisitCreate extends Form {
    constructor(appendEl) {
        super();
        this.appendEl = appendEl;
    }
    createForm() {
        // console.log(this.appendEl);
        super.createForm();
        // let modalForm=document.querySelector("modal__form");
        // modalForm.id="visitDoctor";
        const doctors = ["Кардиолог", "Стоматолог", "Терапевт"];
        new NewSelect("doctors", doctors, "Выберите врача", this.appendEl, "selectDoctors").createSelect();
        let selectDoctors = document.querySelector(".selectDoctors");
        console.log(selectDoctors);
        selectDoctors.addEventListener("change", function() {
            if (document.querySelector(".allDoctors") !== null) {
                document.querySelector(".allDoctors").remove();
            }
            switch (selectDoctors.selectedIndex) {
                case 1:
                    {
                        new VisitCardiologist().createVisit();
                        break;
                    }
                case 2:
                    {
                        new VisitDentist().createVisit();
                        break;
                    }
                case 3:
                    {
                        new VisitTherapist().createVisit();
                        break;
                    }
            }
        });
    }
}

let form = new FormVisitCreate("modalEl");